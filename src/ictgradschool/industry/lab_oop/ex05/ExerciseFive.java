package ictgradschool.industry.lab_oop.ex05;

/**
 * Created by anhyd on 3/03/2017.
 */
public class ExerciseFive {

    /**
     * Runs an example program.
     */
    private void start() {
        String letters = "X X O O X O X O X ";

        String row1 = getRow(letters, 1);

        String row2 = getRow(letters, 2);

        String row3 = getRow(letters, 3);

        printRows(row1, row2, row3);

        String leftDiagonal = String.valueOf(getLeftDiagonal(row1, row2, row3));

        printDiagonal(leftDiagonal);
    }

    /**
     * TODO Implement this
     */
    public String getRow(String letters, int row) {
        return letters.substring(0);
    }

    public void printRows(String row1, String row2, String row3) {
        System.out.println(row1);
        System.out.println(row2);
        System.out.println(row3);

    }

    /**
     * TODO Implement this
     */
    public int getLeftDiagonal(String row1, String row2, String row3) {
        char charOne = row1.charAt(0);
        char charTwo = row2.charAt(6);
        char charThree = row3.charAt(12);

        return charOne + charTwo + charThree;
    }

    /**
     * TODO Implement this
     */
    public void printDiagonal(String leftDiagonal) {

    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        ExerciseFive ex = new ExerciseFive();
        ex.start();
    }
}
