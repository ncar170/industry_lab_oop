package ictgradschool.industry.lab_oop.ex04;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter an amount and a number of decimal places.  The program should then
 * truncate the amount to the user-specified number of decimal places using String methods.
 *
 * <p>To truncate the amount to the user-specified number of decimal places, the String method indexOf() should be used
 * to find the position of the decimal point, and the method substring() should then be used to extract the amount to
 * the user-specified number of decimal places.  The program is to be written so that each task is in a separate method.
 * You need to write four methods, one method for each of the following tasks:</p>
 * <ul>
 *     <li>Printing the prompt and reading the amount from the user</li>
 *     <li>Printing the prompt and reading the number of decimal places from the user</li>
 *     <li>Truncating the amount to the user-specified number of DP's</li>
 *     <li>Printing the truncated amount</li>
 * </ul>
 */
public class ExerciseFour {


    private void start() {

        String userNumber = getNumberFromUser("Please enter an amount:");

        int userDecimal = getDecimalFromUser ("Please enter the number of decimal points:");

        String numberDecimals = truncateDecimalPoints (userNumber, userDecimal);

        printDecimal(userDecimal, numberDecimals);

        // TODO Use other methods you create to implement this program's functionality.
    }

    private String getNumberFromUser(String prompt) {
        System.out.println(prompt);
        return Keyboard.readInput();
    }

    private int getDecimalFromUser (String prompt) {
        System.out.println(prompt);
        return Integer.parseInt(Keyboard.readInput());
    }

    private String truncateDecimalPoints (String number, int decimal) {
        decimal = number.indexOf(".");
        return number.substring(0, decimal);
    }

    private void printDecimal (int decimal, String userNumber) {
        System.out.println("Amount truncated to " + decimal + " decimal places is: " + userNumber);
    }

    // TODO Write a method which truncates the specified number to the specified number of DP's

    // TODO Write a method which prints the truncated amount

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        ExerciseFour ex = new ExerciseFour();
        ex.start();
    }
}
